﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoAdPlane : MonoBehaviour
{
    public AudioClip menuTap;						//touch sound

    private bool canTap;
	private float buttonAnimationSpeed = 9.0f;		//button scale animation speed
	
    private void Start()
    {
        canTap = true;
    }
    
    private void OnEnable()
    {
	    AdManager.Instance.InitAds();
	    AdManager.Instance.LoadRewardedVideo();
	    AdManager.Instance.OnRewardedVideoClosed += onRewardedVideoClosed;
    }

    private void OnDisable()
    {
	    AdManager.Instance.OnRewardedVideoClosed -= onRewardedVideoClosed;
    }

    // Update is called once per frame
    void Update()
    {
        if(canTap)
			tapManager();
    }

    ///***********************************************************************
	/// Manage user taps on gameover buttons
	///***********************************************************************
	private RaycastHit hitInfo;
	private Ray ray;
	private void tapManager() {
		
		//Mouse of touch?
		if(	Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended)  
			ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
		else if (Input.GetMouseButtonUp(0))
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		else
			return;
		
		if (Physics.Raycast(ray, out hitInfo)) {
			GameObject objectHit = hitInfo.transform.gameObject;
			switch(objectHit.name) {
				case "recoverBtn":
                    canTap = true;
                    print("recoverBtn Clicked");
					playSfx(menuTap);									//play audioclip
                    
                    AdManager.Instance.ShowRewardedVideo();

#if UNITY_EDITOR
                    FindObjectOfType<PlayerManager>().SimulateAdPlayFinished();
                    onRewardedVideoClosed();
#endif
                    
					break;
				case "exitBtn":
                    canTap = true;
                    print("exitBtn Clicked");
					playSfx(menuTap);
                    gameObject.SetActive(false);
			        print ("Game Over");
                    GameController.gameOver = true;
                    PauseManager.Instance.UnFreezeGame();
					// SceneManager.LoadScene("Menu");
					break;
                // default:
                //     break;  
				}	
		}
	}

    ///***********************************************************************
	/// IPlay audioclip
	///***********************************************************************
	void playSfx(AudioClip _sfx) {
		GetComponent<AudioSource>().clip = _sfx;
		if(!GetComponent<AudioSource>().isPlaying)
			GetComponent<AudioSource>().Play();
	}

    ///***********************************************************************
	/// Animate buttons on touch
	///***********************************************************************
	IEnumerator animateButton(GameObject _btn) {
		canTap = false;
		Vector3 startingScale = _btn.transform.localScale;
		Vector3 destinationScale = startingScale * 1.1f;

		float t = 0.0f; 
		while (t <= 1.0f) {
			t += Time.deltaTime * buttonAnimationSpeed;
			_btn.transform.localScale = new Vector3(Mathf.SmoothStep(startingScale.x, destinationScale.x, t),
			                                        _btn.transform.localScale.y,
			                                        Mathf.SmoothStep(startingScale.z, destinationScale.z, t));
			yield return 0;
		}
		
		float r = 0.0f; 
		if(_btn.transform.localScale.x >= destinationScale.x) {
			while (r <= 1.0f) {
				r += Time.deltaTime * buttonAnimationSpeed;
				_btn.transform.localScale = new Vector3(Mathf.SmoothStep(destinationScale.x, startingScale.x, r),
				                                        _btn.transform.localScale.y,
				                                        Mathf.SmoothStep(destinationScale.z, startingScale.z, r));
				yield return 0;
			}
		}
		
		if(r >= 1)
			canTap = true;
	}

    void onRewardedVideoClosed()
    {
	    this.gameObject.SetActive(false);
	    GameController.gameOver = false;
    }
}
