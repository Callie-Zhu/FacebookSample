﻿/* Ripped from http://wiki.unity3d.com/index.php/Singleton

	Usage:
		public class Manager : Singleton<Manager> {
			protected Manager () {} // guarantee this will be always a singleton only - can't use the constructor!

			public string myGlobalVar = "whatever";
		}
*/

using UnityEngine;

namespace Utility
{
	/// <summary>
	/// Be aware this will not prevent a non singleton constructor
	///   such as `T myT = new T();`
	/// To prevent that, add `protected T () {}` to your singleton class.
	///
	/// As a note, this is made as MonoBehaviour because we need Coroutines.
	/// </summary>
	public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static T _instance;
		
		private static object _lock = new object();
		
		public static T Instance
		{
			get
			{
				if (applicationIsQuitting)
				{
					Debug.LogWarning("[Singleton] Instance '"+ typeof(T) +
					                 "' already destroyed on application quit." +
					                 " Won't create again - returning null.");
					
					return null;
				}
				
				lock (_lock)
				{
					if (_instance == null)
					{
						_instance = (T)FindObjectOfType(typeof(T));
						
						// Error several instances have been initialized
						if (FindObjectsOfType(typeof(T)).Length > 1)
						{
							Debug.LogError("[Singleton] Something went really wrong " +
							               " - there should never be more than 1 singleton!" +
							               " Reopenning the scene might fix it.");
						}
						
						// No instance in the current scene, we create the missing singleton
						if (_instance == null)
						{
							GameObject newSingleton = new GameObject("_" + typeof(T).Name);
							_instance = newSingleton.AddComponent<T>();
						}
					}
					
					return _instance;
				}
			}
		}

		// Don't destroy on Load
		protected virtual bool KeepAlive
		{
			get { return false; }
		}

		private static bool applicationIsQuitting = false;

		protected bool DuplicatedSingleton 
		{
			get;
			private set;
		}
		
		/// <summary>
		/// When Unity quits, it destroys objects in a random order.
		/// In principle, a Singleton is only destroyed when application quits.
		/// If any script calls Instance after it have been destroyed,
		///   it will create a buggy ghost object that will stay on the Editor scene
		///   even after stopping playing the Application. Really bad!
		/// So, this was made to be sure we're not creating that buggy ghost object.
		/// </summary>
		public virtual void OnApplicationQuit()
		{
			applicationIsQuitting = true;
		}

		protected virtual void Awake()
		{
			if (KeepAlive) 
			{
				// Security to avoid to have two singletons in the same scene
				if(_instance != null && _instance != this)
				{
					Destroy(this.gameObject);
					DuplicatedSingleton = true;
				}

				else
				{
					_instance = this.GetComponent<T>();
					DontDestroyOnLoad (this.gameObject);
					DuplicatedSingleton = false;
				}
			}
		}
	}
}