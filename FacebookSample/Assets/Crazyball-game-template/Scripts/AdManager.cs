﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using AudienceNetwork;
using Utility;

public class AdManager : Singleton<AdManager>
{

#if UNITY_IOS
    
    private static string BANNER_PLACEMENT_ID = "1313998652072611_1314005508738592";
    private static string INTERSTITIAL_PLACEMENT_ID = "1313998652072611_1314001838738959";
    private static string REWARDEDVIDEO_PLACEMENT_ID = "1313998652072611_1486232404849234";

#elif UNITY_ANDROID

    private static string BANNER_PLACEMENT_ID = "1313998652072611_1432822510190224";
    private static string INTERSTITIAL_PLACEMENT_ID = "1313998652072611_1314004482072028";
    private static string REWARDEDVIDEO_PLACEMENT_ID = "1313998652072611_1432803623525446";

#endif

    // Debug Info Text
    public TextMesh debugInfoText;
    // AD View to display Banners
    private AdView adView;
    private AdPosition currentAdViewPosition;
    private ScreenOrientation currentScreenOrientation;

    // Interstitial Ad
    private InterstitialAd interstitialAd;
    public bool isInterstitialLoaded;
    public delegate void onInterstitialClosed();

    private Action interstitialClosed = default;
     
     public event Action OnInterstitialClosed
     {
         add
         {
             if (interstitialClosed != null &&
                 interstitialClosed.GetInvocationList().Contains(value))
             {
                 return;
             }
             
             interstitialClosed += value;
         }

         remove => interstitialClosed -= value;
     }

     private Action rewardedVideoClosed = default;
     
     public event Action OnRewardedVideoClosed
     {
         add
         {
             if (rewardedVideoClosed != null &&
                 rewardedVideoClosed.GetInvocationList().Contains(value))
             {
                 return;
             }

             rewardedVideoClosed += value;
         }

         remove => rewardedVideoClosed -= value;
     }

#pragma warning disable 0414
    private bool didInterstitialClose;
#pragma warning restore 0414

    // Rewarded Video
    private RewardedVideoAd rewardedVideoAd;
    public bool isRewardedVideoLoaded;
    public delegate void onRewardedVideoClosed();
#pragma warning disable 0414
    private bool didRewardedVideoClose;
#pragma warning restore 0414

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        UpdateDebugInfo("");
    }
    public void InitAds()
    {
        if (!AudienceNetworkAds.IsInitialized())
        {
            AudienceNetworkAds.Initialize();
            UpdateDebugInfo("Init AudienceNetwork");
        }
    }

    /// <summary>
    /// Load a Banner and display right after loaded
    /// </summary>
    public void LoadBanner(){
        if (this.adView) {
            this.adView.Dispose();
        }

        this.adView = new AdView(BANNER_PLACEMENT_ID, AdSize.BANNER_HEIGHT_50);
        this.adView.Register(this.gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        this.adView.AdViewDidLoad = (delegate() {
            Debug.Log("Banner loaded.");
#if  UNITY_IOS
            this.adView.Show(30);
#elif UNITY_ANDROID
            this.adView.Show(1);
#endif
        });
        adView.AdViewDidFailWithError = (delegate(string error) {
            Debug.Log("Banner failed to load with error: " + error);
        });
        adView.AdViewWillLogImpression = (delegate() {
            Debug.Log("Banner logged impression.");
        });
        adView.AdViewDidClick = (delegate() {
            Debug.Log("Banner clicked.");
        });

        // Initiate a request to load an ad.
        adView.LoadAd();
    }

    /// <summary>
    /// Load an interstitial ad
    /// </summary>
    public void LoadInterstitial(){
         // Create the interstitial unit with a placement ID (generate your own on the Facebook app settings).
        // Use different ID for each ad placement in your app.
        interstitialAd = new InterstitialAd(INTERSTITIAL_PLACEMENT_ID);
        interstitialAd.Register(gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        interstitialAd.InterstitialAdDidLoad = delegate () {
            UpdateDebugInfo("Interstitial ad loaded.");
            isInterstitialLoaded = true;
            didInterstitialClose = false;
            string isAdValid = interstitialAd.IsValid() ? "valid" : "invalid";
            Debug.Log("Ad loaded and is " + isAdValid + ". Click show to present!");
        };
        interstitialAd.InterstitialAdDidFailWithError = delegate (string error) {
            UpdateDebugInfo("Interstitial failed to load error: " + error);
            Debug.Log("Interstitial ad failed to load. Check console for details.");
            LoadInterstitial();
            if(interstitialClosed != null){
                interstitialClosed();
            }
        };
        interstitialAd.InterstitialAdWillLogImpression = delegate () {
            UpdateDebugInfo("Interstitial ad logged impression.");
        };
        interstitialAd.InterstitialAdDidClick = delegate () {
            UpdateDebugInfo("Interstitial ad clicked.");
        };
        interstitialAd.InterstitialAdDidClose = delegate() {
            UpdateDebugInfo("Interstitial ad did close.");
            didInterstitialClose = true;
            if (interstitialAd != null) {
                interstitialAd.Dispose();
            }
            if(interstitialClosed != null){
                interstitialClosed();
            }
        };

#if UNITY_ANDROID
        /*
         * Only relevant to Android.
         * This callback will only be triggered if the Interstitial activity has
         * been destroyed without being properly closed. This can happen if an
         * app with launchMode:singleTask (such as a Unity game) goes to
         * background and is then relaunched by tapping the icon.
         */
        interstitialAd.interstitialAdActivityDestroyed = delegate() {
            if (!didInterstitialClose) {
                UpdateDebugInfo("Interstitial activity destroyed without being closed first.");
                Debug.Log("Game should resume.");
            }
        };
#endif

        // Initiate the request to load the ad.
        interstitialAd.LoadAd();
    }

    /// <summary>
    /// Load a rewarded video ad
    /// </summary>
    public void LoadRewardedVideo(){
        // Create the rewarded video unit with a placement ID (generate your own on the Facebook app settings).
        // Use different ID for each ad placement in your app.
        rewardedVideoAd = new RewardedVideoAd(REWARDEDVIDEO_PLACEMENT_ID);
        // For S2S validation you can create the rewarded video ad with the reward data
        // Refer to documentation here:
        // https://developers.facebook.com/docs/audience-network/android/rewarded-video#server-side-reward-validation
        // https://developers.facebook.com/docs/audience-network/ios/rewarded-video#server-side-reward-validation
        RewardData rewardData = new RewardData
        {
            UserId = "callie",
            Currency = "recover"
        };
#pragma warning disable 0219
        RewardedVideoAd s2sRewardedVideoAd = new RewardedVideoAd(REWARDEDVIDEO_PLACEMENT_ID, rewardData);
#pragma warning restore 0219

        rewardedVideoAd.Register(gameObject);
        // Set delegates to get notified on changes or when the user interacts with the ad.
        rewardedVideoAd.RewardedVideoAdDidLoad = delegate ()
        {
            UpdateDebugInfo("RewardedVideo ad loaded.");
            isRewardedVideoLoaded = true;
            didRewardedVideoClose = false;
            string isAdValid = rewardedVideoAd.IsValid() ? "valid" : "invalid";
            Debug.Log("Ad loaded and is " + isAdValid + ". Click show to present!");
        };
        rewardedVideoAd.RewardedVideoAdDidFailWithError = delegate (string error)
        {
            UpdateDebugInfo("Rewarded ad failed to load with error: " + error);
            Debug.Log("RewardedVideo ad failed to load. Check console for details.");
        };
        rewardedVideoAd.RewardedVideoAdWillLogImpression = delegate ()
        {
            UpdateDebugInfo("RewardedVideo ad logged impression.");
        };
        rewardedVideoAd.RewardedVideoAdDidClick = delegate ()
        {
            UpdateDebugInfo("RewardedVideo ad clicked.");
        };

        // For S2S validation you need to register the following two callback
        // Refer to documentation here:
        // https://developers.facebook.com/docs/audience-network/android/rewarded-video#server-side-reward-validation
        // https://developers.facebook.com/docs/audience-network/ios/rewarded-video#server-side-reward-validation
        rewardedVideoAd.RewardedVideoAdDidSucceed = delegate ()
        {
            UpdateDebugInfo("Rewarded video ad validated by server");
        };

        rewardedVideoAd.RewardedVideoAdDidFail = delegate ()
        {
            UpdateDebugInfo("Rewarded video ad not validated, or no response from server");

            if(rewardedVideoClosed != null){
                rewardedVideoClosed();
            }
        };

        rewardedVideoAd.RewardedVideoAdDidClose = delegate ()
        {
            UpdateDebugInfo("Rewarded video ad did close.");
            didRewardedVideoClose = true;
            if (rewardedVideoAd != null)
            {
                rewardedVideoAd.Dispose();
            }
            if(rewardedVideoClosed != null){
                rewardedVideoClosed();
            }
        };

#if UNITY_ANDROID
        /*
         * Only relevant to Android.
         * This callback will only be triggered if the Rewarded Video activity
         * has been destroyed without being properly closed. This can happen if
         * an app with launchMode:singleTask (such as a Unity game) goes to
         * background and is then relaunched by tapping the icon.
         */
        rewardedVideoAd.RewardedVideoAdActivityDestroyed = delegate ()
        {
            if (!didRewardedVideoClose)
            {
                UpdateDebugInfo("Rewarded video activity destroyed without being closed first.");
                Debug.Log("Game should resume. User should not get a reward.");
            }
        };
#endif

        // Initiate the request to load the ad.
        rewardedVideoAd.LoadAd();
    }

    /// <summary>
    /// Destory a banner when needed
    /// </summary>
    public void HideBanner(){
        UpdateDebugInfo("Hide Banner");
        if(this.adView == null) return;
        this.adView.Dispose();
    }

    /// <summary>
    /// Show interstitial ad
    /// </summary>
    public void ShowInterstitial(){
        UpdateDebugInfo("Show Interstitial");
        if(isInterstitialLoaded){
            isInterstitialLoaded = false;
            interstitialAd.Show();
        }
        else{
            UpdateDebugInfo("Interstitial Ad Not Loaded. Load an ad first!");
        }
    }

    /// <summary>
    /// Show rewarded video ad
    /// </summary>
    public void ShowRewardedVideo(){
        UpdateDebugInfo("Show Rewarded Video");
        if(isRewardedVideoLoaded){
            isRewardedVideoLoaded = false;
            rewardedVideoAd.Show();
        }
        else{
            UpdateDebugInfo("Rewarded Ad Not Loaded. Load an ad first!");
        }
    }

    /// <summary>
    /// This function is called when the MonoBehaviour will be destroyed.
    /// </summary>
    private void OnDestroy()
    {
        if(interstitialAd != null){
            interstitialAd.Dispose();
        }
        if(rewardedVideoAd != null){
            rewardedVideoAd.Dispose();
        }
    }

    public void UpdateDebugInfo(string message){
        if(debugInfoText != null){
            debugInfoText.text = message;
        }
    }
}
