﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Utility;

public class PauseManager : Singleton<PauseManager>
{
    /// <summary>
    /// This class manages pause and unpause states.
    /// </summary> 
    public GameObject pausePlane; //we move this plane over all other elements in the game to simulate the pause

    public bool IsPaused => isPaused;

    private enum Page
    {
        PLAYING,
        PAUSE
    }

    private bool isPaused; //is game already paused?
    private Page currentPauseState = Page.PLAYING;


    void Awake()
    {
        isPaused = false;
        Time.timeScale = 1.0f;
        if (pausePlane)
            pausePlane.SetActive(false);
    }


    void Update()
    {
        //touch control
        touchManager();

        //optional pause
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyUp(KeyCode.Escape))
        {
            //PAUSE THE GAME
            switch (currentPauseState)
            {
                case Page.PLAYING:
                    PauseGame();
                    break;
                case Page.PAUSE:
                    UnPauseGame();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
                    break;
            }
        }

        //debug restart
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }


    private RaycastHit hitInfo;
    private Ray ray;

    void touchManager()
    {
        //Mouse of touch?
        if (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended)
            ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
        else if (Input.GetMouseButtonUp(0))
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        else
            return;

        if (Physics.Raycast(ray, out hitInfo))
        {
            GameObject objectHit = hitInfo.transform.gameObject;
            switch (objectHit.name)
            {
                case "pauseBtn":
                    switch (currentPauseState)
                    {
                        case Page.PLAYING:
                            PauseGame();
                            break;
                        case Page.PAUSE:
                            UnPauseGame();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                            break;
                    }

                    break;

                case "retryButtonPause":
                    UnPauseGame();
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                    break;

                case "menuButtonPause":
                    UnPauseGame();
                    SceneManager.LoadScene("Menu");
                    break;
            }
        }
    }

    public void FreezeGame()
    {
        isPaused = true;
        Time.timeScale = 0;
        AudioListener.volume = 0;
    }

    public void UnFreezeGame()
    {
        isPaused = false;
        Time.timeScale = 1.0f;
        AudioListener.volume = 1.0f;
    }
    
    public void PauseGame()
    {
        print("Game is Paused...");

        FreezeGame();

        if (pausePlane)
            pausePlane.SetActive(true);

        currentPauseState = Page.PAUSE;
    }

    public void UnPauseGame()
    {
        print("Unpause");
        
        UnFreezeGame();

        if (pausePlane)
            pausePlane.SetActive(false);

        currentPauseState = Page.PLAYING;
    }
}