﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerManager : MonoBehaviour
{
    /// <summary>
    /// Main Player manager class.
    /// We check all player collisions here.
    /// We also calculate the score in this class. 
    /// </summary>
    public static int playerScore; //player score

    public GameObject scoreTextDynamic; //gameobject which shows the score on screen
    public GameObject videoAdPlane; // Pop up when game failed

    private bool receivingPlayerCollisionMessage = true;

    void Awake()
    {
        playerScore = 0;
        //Disable screen dimming on mobile devices
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //Playe the game with a fixed framerate in all platforms
        Application.targetFrameRate = 60;
    }

    private void OnEnable()
    {
        AdManager.Instance.InitAds();
        AdManager.Instance.LoadInterstitial();
        AdManager.Instance.LoadRewardedVideo();
        AdManager.Instance.OnInterstitialClosed += OnInterstitialAdClosed;
        AdManager.Instance.OnRewardedVideoClosed += OnRewardedVideoClosed;
    }

    private void OnDisable()
    {
        AdManager.Instance.OnInterstitialClosed -= OnInterstitialAdClosed;
        AdManager.Instance.OnRewardedVideoClosed -= OnRewardedVideoClosed;
    }

    void Update()
    {
        if (!GameController.gameOver && !PauseManager.Instance.IsPaused)
            calculateScore();
    }

    ///***********************************************************************
    /// calculate player's score
    /// Score is a combination of gameplay duration (while player is still alive) 
    /// and a multiplier for the current level.
    ///***********************************************************************
    void calculateScore()
    {
        playerScore += (int) (GameController.currentLevel * Mathf.Log(GameController.currentLevel + 1, 2));
        scoreTextDynamic.GetComponent<TextMesh>().text = playerScore.ToString();
    }

    ///***********************************************************************
    /// Collision detection and management
    ///***********************************************************************
    void OnCollisionEnter(Collision c)
    {
        if (!receivingPlayerCollisionMessage) return;

        receivingPlayerCollisionMessage = false;
        
        //collision with mazes and enemy balls leads to a sudden game over
        if (c.gameObject.CompareTag("Maze"))
        {
            Destroy(c.gameObject);
            if (AdManager.Instance.isInterstitialLoaded && AdManager.Instance.isRewardedVideoLoaded)
            {
                if (Random.Range(0, 10) > 5)
                {
                    PauseManager.Instance.FreezeGame();
                    // Pop up video ad plane
                    if (videoAdPlane != null)
                    {
                        videoAdPlane.SetActive(true);
                    }
                }
                else
                {
                    PauseManager.Instance.FreezeGame();
                
                    if (videoAdPlane != null)
                    {
                        videoAdPlane.SetActive(false);
                    }

                    AdManager.Instance.ShowInterstitial();
#if UNITY_EDITOR
                    OnInterstitialAdClosed();
#endif
                }
            }
            else
            {
                if (videoAdPlane != null)
                {
                    videoAdPlane.SetActive(false);
                }

                GameController.gameOver = true;
            }
        }

        if (c.gameObject.tag == "enemyBall")
        {
            Destroy(c.gameObject);
        }
    }

    void playSfx(AudioClip _sfx)
    {
        GetComponent<AudioSource>().clip = _sfx;
        if (!GetComponent<AudioSource>().isPlaying)
            GetComponent<AudioSource>().Play();
    }

    void OnInterstitialAdClosed()
    {
        PauseManager.Instance.UnFreezeGame();
        GameController.gameOver = true;
    }

    void OnRewardedVideoClosed()
    {
        PauseManager.Instance.UnFreezeGame();
        receivingPlayerCollisionMessage = true;
    }

    public void SimulateAdPlayFinished()
    {
        OnRewardedVideoClosed();
    }
}